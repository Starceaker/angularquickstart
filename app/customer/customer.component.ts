import { Component, Input, OnInit } from '@angular/core';

@Component({
    moduleId: module.id,
    selector: 'app-customer',
    templateUrl: 'customer.component.html'
})
export class CustomerComponent implements OnInit {
    @Input() customer: {id: number, name: string}; //this is the type, same as in swift the ": ***" means the type of the object.

    constructor() { }

    ngOnInit() { }

    anyColor = "purple";
}