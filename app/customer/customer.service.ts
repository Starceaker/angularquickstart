import { Injectable } from '@angular/core';

@Injectable()
export class CustomerService {

    constructor() { }

    getCustomers(){
        return [
        {id: 1, name: "Clint"},
        {id: 2, name: "Emmet"},
        {id: 3, name: "Julie"},
        {id: 4, name: "Wouter"},
        {id: 5, name: "Vasileios"}        
    ];
    }
}
