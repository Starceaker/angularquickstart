import { Component, OnInit } from '@angular/core';
import {CustomerService} from './customer.service';

@Component({
    moduleId: module.id,
    selector: 'app-customers',
    templateUrl: 'customers.component.html'
})
export class CustomersComponent implements OnInit {
    customers: any[];
    
    constructor(private _customerService: CustomerService) { } //TODO: implement dependency injection because Angular doesn't know what CustomerService is (yet)

    ngOnInit() {
        this.customers = this._customerService.getCustomers();
     }

    /*customers = [
        {id: 1, name: "Clint"},
        {id: 2, name: "Emmet"},
        {id: 3, name: "Julie"},
        {id: 4, name: "Wouter"},
        {id: 5, name: "Vasileios"}        
    ];*/
}
