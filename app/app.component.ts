import { Component } from '@angular/core';

@Component({
    moduleId: module.id,
    selector: 'my-app',    
    templateUrl: 'app.component.html'    
})
export class AppComponent {

// [] means property binding - Component to DOM
// () means event binding - DOM to Component

    title = "Starceaker's Customer app";
    name = "Clint";
    anyColor = "blue";
    
    changeColor(){
        this.anyColor = this.anyColor === "blue" ? "red" : "blue";
    }
 }
